<?php

namespace Ensi\SellerAuthClient;

class SellerAuthClientProvider
{
    /** @var string[] */
    public static $apis = ['\Ensi\SellerAuthClient\Api\OauthApi', '\Ensi\SellerAuthClient\Api\UsersApi'];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\SellerAuthClient\Dto\AddRolesToUserRequest',
        '\Ensi\SellerAuthClient\Dto\CreateTokenRequest',
        '\Ensi\SellerAuthClient\Dto\CreateTokenResponse',
        '\Ensi\SellerAuthClient\Dto\CreateUserRequest',
        '\Ensi\SellerAuthClient\Dto\DeleteRoleFromUserRequest',
        '\Ensi\SellerAuthClient\Dto\EmptyDataResponse',
        '\Ensi\SellerAuthClient\Dto\Error',
        '\Ensi\SellerAuthClient\Dto\ErrorResponse',
        '\Ensi\SellerAuthClient\Dto\ErrorResponse2',
        '\Ensi\SellerAuthClient\Dto\ExtendedPatchUserRequest',
        '\Ensi\SellerAuthClient\Dto\FilterUsersRequest',
        '\Ensi\SellerAuthClient\Dto\GrantTypeEnum',
        '\Ensi\SellerAuthClient\Dto\ModelInterface',
        '\Ensi\SellerAuthClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\SellerAuthClient\Dto\PaginationTypeEnum',
        '\Ensi\SellerAuthClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\SellerAuthClient\Dto\PatchUserRequest',
        '\Ensi\SellerAuthClient\Dto\RequestBodyCursorPagination',
        '\Ensi\SellerAuthClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\SellerAuthClient\Dto\RequestBodyPagination',
        '\Ensi\SellerAuthClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\SellerAuthClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\SellerAuthClient\Dto\ResponseBodyPagination',
        '\Ensi\SellerAuthClient\Dto\Role',
        '\Ensi\SellerAuthClient\Dto\RoleEnum',
        '\Ensi\SellerAuthClient\Dto\RoleResponse',
        '\Ensi\SellerAuthClient\Dto\SearchRolesRequest',
        '\Ensi\SellerAuthClient\Dto\SearchRolesResponse',
        '\Ensi\SellerAuthClient\Dto\SearchUsersRequest',
        '\Ensi\SellerAuthClient\Dto\SearchUsersResponse',
        '\Ensi\SellerAuthClient\Dto\SearchUsersResponseMeta',
        '\Ensi\SellerAuthClient\Dto\User',
        '\Ensi\SellerAuthClient\Dto\UserFillableProperties',
        '\Ensi\SellerAuthClient\Dto\UserIncludes',
        '\Ensi\SellerAuthClient\Dto\UserReadonlyProperties',
        '\Ensi\SellerAuthClient\Dto\UserResponse',
        '\Ensi\SellerAuthClient\Dto\UserRole',
        '\Ensi\SellerAuthClient\Dto\UserRoleAllOf',
        '\Ensi\SellerAuthClient\Dto\UserWriteOnlyProperties',
    ];

    /** @var string */
    public static $configuration = '\Ensi\SellerAuthClient\Configuration';
}
