# Ensi\SellerAuthClient\UsersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addRolesToUser**](UsersApi.md#addRolesToUser) | **POST** /users/{id}:add-roles | Добавление ролей пользователю
[**createUser**](UsersApi.md#createUser) | **POST** /users | Создание объекта типа User
[**deleteRoleFromUser**](UsersApi.md#deleteRoleFromUser) | **POST** /users/{id}:delete-role | Удаление роли у пользователя
[**deleteUser**](UsersApi.md#deleteUser) | **DELETE** /users/{id} | Удаление объекта типа User
[**extendedPatchUser**](UsersApi.md#extendedPatchUser) | **PATCH** /users/{id}:extended | Обновления части полей объекта типа User с дополнительной фильтрацией
[**getCurrentUser**](UsersApi.md#getCurrentUser) | **POST** /users:current | Получить текущего пользователя
[**getRole**](UsersApi.md#getRole) | **GET** /roles/{id} | Получение объекта типа Role
[**getUser**](UsersApi.md#getUser) | **GET** /users/{id} | Получение объекта типа User
[**patchUser**](UsersApi.md#patchUser) | **PATCH** /users/{id} | Обновления части полей объекта типа User
[**searchRoles**](UsersApi.md#searchRoles) | **POST** /roles:search | Поиск объектов типа Role
[**searchUser**](UsersApi.md#searchUser) | **POST** /users:search-one | Поиск объекта типа User
[**searchUsers**](UsersApi.md#searchUsers) | **POST** /users:search | Поиск объектов типа User



## addRolesToUser

> \Ensi\SellerAuthClient\Dto\EmptyDataResponse addRolesToUser($id, $add_roles_to_user_request)

Добавление ролей пользователю

Добавление ролей пользователю

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$add_roles_to_user_request = new \Ensi\SellerAuthClient\Dto\AddRolesToUserRequest(); // \Ensi\SellerAuthClient\Dto\AddRolesToUserRequest | 

try {
    $result = $apiInstance->addRolesToUser($id, $add_roles_to_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->addRolesToUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **add_roles_to_user_request** | [**\Ensi\SellerAuthClient\Dto\AddRolesToUserRequest**](../Model/AddRolesToUserRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createUser

> \Ensi\SellerAuthClient\Dto\UserResponse createUser($create_user_request)

Создание объекта типа User

Создание объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_user_request = new \Ensi\SellerAuthClient\Dto\CreateUserRequest(); // \Ensi\SellerAuthClient\Dto\CreateUserRequest | 

try {
    $result = $apiInstance->createUser($create_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->createUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_user_request** | [**\Ensi\SellerAuthClient\Dto\CreateUserRequest**](../Model/CreateUserRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteRoleFromUser

> \Ensi\SellerAuthClient\Dto\EmptyDataResponse deleteRoleFromUser($id, $delete_role_from_user_request)

Удаление роли у пользователя

Удаление роли у пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$delete_role_from_user_request = new \Ensi\SellerAuthClient\Dto\DeleteRoleFromUserRequest(); // \Ensi\SellerAuthClient\Dto\DeleteRoleFromUserRequest | 

try {
    $result = $apiInstance->deleteRoleFromUser($id, $delete_role_from_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->deleteRoleFromUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **delete_role_from_user_request** | [**\Ensi\SellerAuthClient\Dto\DeleteRoleFromUserRequest**](../Model/DeleteRoleFromUserRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteUser

> \Ensi\SellerAuthClient\Dto\EmptyDataResponse deleteUser($id, $filter_users_request)

Удаление объекта типа User

Удаление объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$filter_users_request = new \Ensi\SellerAuthClient\Dto\FilterUsersRequest(); // \Ensi\SellerAuthClient\Dto\FilterUsersRequest | 

try {
    $result = $apiInstance->deleteUser($id, $filter_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->deleteUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **filter_users_request** | [**\Ensi\SellerAuthClient\Dto\FilterUsersRequest**](../Model/FilterUsersRequest.md)|  | [optional]

### Return type

[**\Ensi\SellerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## extendedPatchUser

> \Ensi\SellerAuthClient\Dto\UserResponse extendedPatchUser($id, $extended_patch_user_request)

Обновления части полей объекта типа User с дополнительной фильтрацией

Обновления части полей объекта типа User с дополнительной фильтрацией

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$extended_patch_user_request = new \Ensi\SellerAuthClient\Dto\ExtendedPatchUserRequest(); // \Ensi\SellerAuthClient\Dto\ExtendedPatchUserRequest | 

try {
    $result = $apiInstance->extendedPatchUser($id, $extended_patch_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->extendedPatchUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **extended_patch_user_request** | [**\Ensi\SellerAuthClient\Dto\ExtendedPatchUserRequest**](../Model/ExtendedPatchUserRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCurrentUser

> \Ensi\SellerAuthClient\Dto\UserResponse getCurrentUser()

Получить текущего пользователя

Поиск объектов типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Ensi\SellerAuthClient\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCurrentUser();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getCurrentUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\SellerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRole

> \Ensi\SellerAuthClient\Dto\RoleResponse getRole($id)

Получение объекта типа Role

Получение объекта типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getRole($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\SellerAuthClient\Dto\RoleResponse**](../Model/RoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getUser

> \Ensi\SellerAuthClient\Dto\UserResponse getUser($id, $include)

Получение объекта типа User

Получение объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getUser($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\SellerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchUser

> \Ensi\SellerAuthClient\Dto\UserResponse patchUser($id, $patch_user_request)

Обновления части полей объекта типа User

Обновления части полей объекта типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_user_request = new \Ensi\SellerAuthClient\Dto\PatchUserRequest(); // \Ensi\SellerAuthClient\Dto\PatchUserRequest | 

try {
    $result = $apiInstance->patchUser($id, $patch_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->patchUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_user_request** | [**\Ensi\SellerAuthClient\Dto\PatchUserRequest**](../Model/PatchUserRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRoles

> \Ensi\SellerAuthClient\Dto\SearchRolesResponse searchRoles($search_roles_request)

Поиск объектов типа Role

Поиск объектов типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_roles_request = new \Ensi\SellerAuthClient\Dto\SearchRolesRequest(); // \Ensi\SellerAuthClient\Dto\SearchRolesRequest | 

try {
    $result = $apiInstance->searchRoles($search_roles_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchRoles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_roles_request** | [**\Ensi\SellerAuthClient\Dto\SearchRolesRequest**](../Model/SearchRolesRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\SearchRolesResponse**](../Model/SearchRolesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUser

> \Ensi\SellerAuthClient\Dto\UserResponse searchUser($search_users_request)

Поиск объекта типа User

Поиск объектов типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_users_request = new \Ensi\SellerAuthClient\Dto\SearchUsersRequest(); // \Ensi\SellerAuthClient\Dto\SearchUsersRequest | 

try {
    $result = $apiInstance->searchUser($search_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_users_request** | [**\Ensi\SellerAuthClient\Dto\SearchUsersRequest**](../Model/SearchUsersRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUsers

> \Ensi\SellerAuthClient\Dto\SearchUsersResponse searchUsers($search_users_request)

Поиск объектов типа User

Поиск объектов типа User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\SellerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_users_request = new \Ensi\SellerAuthClient\Dto\SearchUsersRequest(); // \Ensi\SellerAuthClient\Dto\SearchUsersRequest | 

try {
    $result = $apiInstance->searchUsers($search_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_users_request** | [**\Ensi\SellerAuthClient\Dto\SearchUsersRequest**](../Model/SearchUsersRequest.md)|  |

### Return type

[**\Ensi\SellerAuthClient\Dto\SearchUsersResponse**](../Model/SearchUsersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

