# # ExtendedPatchUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**\Ensi\SellerAuthClient\Dto\PatchUserRequest**](PatchUserRequest.md) |  | [optional] 
**filter** | [**object**](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


