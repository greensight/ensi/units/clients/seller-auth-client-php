# # User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор пользователя | 
**full_name** | **string** | Полное ФИО | 
**short_name** | **string** | Сокращенное ФИО | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата регистрации | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**seller_id** | **int** | id продавца | [optional] 
**active** | **bool** | Активен | [optional] 
**login** | **string** | Логин | [optional] 
**last_name** | **string** | Фамилия | [optional] 
**first_name** | **string** | Имя | [optional] 
**middle_name** | **string** | Отчество | [optional] 
**email** | **string** | Email | [optional] 
**phone** | **string** | Телефон | [optional] 
**roles** | [**\Ensi\SellerAuthClient\Dto\UserRole[]**](UserRole.md) | Роли пользователя. Объект, в котором ключ - ID роли | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


