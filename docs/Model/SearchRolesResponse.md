# # SearchRolesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\SellerAuthClient\Dto\Role[]**](Role.md) |  | 
**meta** | [**\Ensi\SellerAuthClient\Dto\SearchUsersResponseMeta**](SearchUsersResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


